@extends('layouts.core')
@section('meta')
    <script type='text/javascript' src='https://platform-api.sharethis.com/js/sharethis.js#property=61affcc4e4bc6d0019c1ca28&product=sop' async='async'></script>
    <meta name="description" content="{!! Str::words(strip_tags($post->content), '30') !!}" />
    <title>Coca Cola - Əsl Möcüzə - {{$post->title}}</title>
    <meta property="og:title" content="Coca Cola - Əsl Möcüzə - {{$post->title}}" />
    <meta property="og:url" content="{{ env('APP_URL') }}/posts/{{$post->slug}}" />
    <meta property="og:type" content="article" />
    <meta property="og:image" content="{{ env('APP_URL') }}/images/posts/{{$post->images[0]->image}}" />
    <meta property="og:image:secure_url" content="{{ env('APP_URL') }}/images/posts/{{$post->images[0]->image}}" />
    <meta property="og:image:alt" content="Coca Cola - Əsl Möcüzə - {{$post->title}}" />
    <meta property="og:description" content="{!! Str::words(strip_tags($post->content), '30') !!}" />
@endsection
@section('content')
    <!-- ========== Start Single Post ========== -->

    <section class="main-content post-single">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-12">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="post-item">
                                <div class="post-header">
                                    <h3 class="post-title">{{$post->title}}</h3>
                                    <span class="post-date">
                                        <i class="fa fa-calendar"></i>
                                        <span>{{$post->created_at->diffForHumans()}}</span>
                                    </span>

                                    <span class="post-date">
                                        <i class="fa fa-book"></i>
                                        <a href="/categories/{{$post->category->id}}">{{$post->category->title}}</a>
                                    </span>
                                </div>
                                <div class="post-content">
                                    @foreach($post->images as $image)
                                        @if($loop->first)
                                            <img src="/images/posts/{{$image->image}}" alt="{{$post->title}}" style="width: 220px; float: left">
                                        @endif
                                    @endforeach
                                    {!! $post->content !!}

                                    @if(!empty($post->file))
                                        <a href="">
                                            <i class="fa fa-file"></i> Download PDF
                                        </a>
                                    @endif
                                        <br><br>
                                    <div class="sharethis-inline-share-buttons"></div><!-- ShareThis END -->
                                </div>

                                <style>
                                    .post-images {
                                        margin: 20px 0 0;
                                    }
                                    .post-image {
                                        position: relative;
                                        padding: 0 0 60%;
                                    }
                                    .post-image img {
                                        position: absolute;
                                        left: 0;
                                        top: 0;
                                        width: 100%;
                                        height: 100%;
                                        object-fit: cover;
                                        object-position: center;
                                        border-radius: 10px;
                                    }
                                </style>

                                <div class="row post-images">
                                    @foreach($post->images as $image)
                                        @if(!$loop->first)
                                        <div class="col-md-3">
                                            <div class="post-thumbnail post-image">
                                                <img class="img-responsive" src="/images/posts/{{$image->image}}" alt="">
                                            </div>
                                        </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <div class="comments">
                                <h3 class="comments-count">{{count($post->comments)}} Comments</h3>
                                <ul class="list-unstyled comments-list">
                                    @foreach($post->comments as $comment)
                                    <li class="comment">
                                        <div class="comment-body">
                                            <div class="comment-author">
                                                <strong class="fn">{{$comment->name}}</strong>
                                                <span class="says">says:</span>
                                            </div>
                                            <div class="comment-meta commentmetadata">
                                                <span>{{$comment->created_at->diffForHumans()}}</span>
                                            </div>
                                            <p>{{$comment->comment}}</p>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                                <div id="respond" class="comment-respond">
                                    <h3 class="comment-reply-title">Leave a Reply</h3>
                                    {!! Form::open(['route' => ['addComments',$post->id], 'method' => 'POST', 'class'=> 'comment-form']) !!}
                                        <label class="label">Comment</label>
                                        <textarea id="comment" name="comment" placeholder="Comment*" required="required"></textarea>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label class="label">Name</label>
                                                <input type="text" id="name" name="name" placeholder="Name*" required>
                                            </div>
                                            <div class="col-sm-6">
                                                <label class="label">Email(Your email address will not be published)</label>
                                                <input type="text" id="email" name="email" placeholder="Email*" required>
                                            </div>
                                        </div>
                                        <p class="form-submit">
                                            <input name="submit" type="submit" id="submit" class="submit" value="Post Comment">
                                        </p>
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                @include('sidebar')
            </div>
        </div>
    </section>

@endsection
