<div class="fullWidthSection">
    <div class="column">
        <div class="newsContent">
            <h2>{{$post->title}}</h2>
            <p>{!! Str::words(strip_tags($post->content), '30') !!}</p>
            <div class="buttonWrapper">
                <a href="/posts/{{$post->slug}}" class="button">Ətraflı Oxu</a>
            </div>
        </div>
    </div>
    <div class="column image">
        <img src="/images/news/news.jpg" alt="{{$post->title}}">
    </div>
</div>
