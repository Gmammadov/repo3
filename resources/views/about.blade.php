@extends('layouts.core')
@section('content')
    <section class="main-content single-page about">
        <div class="container">
            <div class="post-item">
                <div class="post-header">
                    <h3 class="post-title">About</h3>
                </div>
                <div class="post-thumbnail">
                    <img class="img-responsive" src="https://via.placeholder.com/1170x780" alt="" >
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1 col-sm-offset-">
                        <div class="post-content">
                            <p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit. Sed maximus orci ac condi mentum efficitur. Suspendi potenti. Fusce diam felis, ullamcor aca felis sed, volutpat varius tortor. Ut eleifend justo sed quam blandit, vehicula ante hendrerit. Sed condimentum libero vel eros porta, eu malesuada nulla bibendum. Proin varius sollicitudin nulla quis fermentum. Nunc vitae arcu eget diam gravida ultrices finibus nec mi. Maecenas egestas libero.</p>
                            <p>Donec ultricies convallis urna. Morbi consequat vestibulum nunc sed semper. Proin iaculis risus eleifend, efficitur eros et, tristique tortor. Integer nec lacinia augue. Curabitur mattis vel orci id mattis. Aliquam eu dignissim sem. Interdum et malesuada fames ac ante ipsum primis in faucibus. Mauris vitae fermentum quam.</p>
                            <div class="personal-info">
                                <p><span><i class="fa fa-user"></i></span>Name: Leila Smith</p>
                                <p><span><i class="fa fa-phone"></i></span>Phone: +1 212 442-7584</p>
                                <p><span><i class="fa fa-envelope"></i></span>Email: Leilasmith@infos.com</p>
                                <p><span><i class="fa fa-map-marker"></i></span>Address: New York, USA</p>
                            </div>
                            <p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit. Sed maximus orci ac condi mentum efficitur. Suspendi potenti. Fusce diam felis, ullamcor aca felis sed, volutpat varius tortor. Ut eleifend justo sed quam blandit, vehicula ante hendrerit. Sed condimentum libero vel eros porta, eu malesuada nulla bibendum. Proin varius sollicitudin nulla quis fermentum. Nunc vitae arcu eget diam gravida ultrices finibus nec mi. Maecenas egestas libero.</p>
                            <blockquote>
                                <p>If you want to live a happy life, tie it to a goal, not to people or things.</p>
                                <p><cite>Albert Einstein</cite></p>
                            </blockquote>
                            <p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit. Sed maximus orci ac condi mentum efficitur. Suspendi potenti. Fusce diam felis, ullamcor aca felis sed, volutpat varius tortor. Ut eleifend justo sed quam blandit, vehicula ante hendrerit. Sed condimentum libero vel eros porta, eu malesuada nulla bibendum. Proin varius sollicitudin nulla quis fermentum. Nunc vitae arcu eget diam gravida ultrices finibus nec mi. Maecenas egestas libero.</p>
                            <p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit. Sed maximus orci ac condi mentum efficitur. Suspendi potenti. Fusce diam felis, ullamcor aca felis sed, volutpat varius tortor. Ut eleifend justo sed quam blandit, vehicula ante hendrerit. Sed condimentum libero vel eros porta, eu malesuada nulla bibendum. Proin varius sollicitudin nulla quis fermentum. Nunc vitae arcu eget diam gravida ultrices finibus nec mi. Maecenas egestas libero.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
