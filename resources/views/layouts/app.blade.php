<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/grid.min.css') }}">

        <!-- Scripts -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>
    <body class="font-sans antialiased">
        <div class="min-h-screen bg-gray-100">
            @include('layouts.navigation')

            <!-- Page Heading -->
            <header class="bg-white shadow">
                <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    {{ $header }}
                </div>
            </header>

            <!-- Page Content -->
            <main>
                {{ $slot }}
            </main>
        </div>
        <script src="/ckeditor/ckeditor.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
        <script type="text/javascript">
            $(document).ready(function() {

                $('.textarea').each(function(e){
                    CKEDITOR.replace(this.id, {
                        height: 300,
                        filebrowserBrowseUrl: "{{asset('/dashboard/file_browser')}}"
                    });
                });

            });
            @if(count($errors) > 0)
            @foreach($errors->all() as $error)
            Swal.fire({
                title: 'Error!',
                text: '{{$error}}',
                icon: 'error',
                toast: true,
                position: 'top-end',
                timerProgressBar: true,
                timer: 2000,
                showConfirmButton: false
            });
            @endforeach
            @endif

            @if(session('success'))
            Swal.fire({
                title: 'Success!',
                text: '{{session('success')}}',
                icon: 'success',
                toast: true,
                position: 'top-end',
                timerProgressBar: true,
                timer: 2000,
                showConfirmButton: false
            })
            @endif

            @if(session('error'))
            Swal.fire({
                title: 'Error!',
                text: '{{session('error')}}',
                icon: 'error',
                toast: true,
                position: 'top-end',
                timerProgressBar: true,
                timer: 2000,
                showConfirmButton: false
            })
            @endif
        </script>
    </body>
</html>
