@extends('layouts.core')
@section('meta')
    <meta name="description" content="“Əsl Möcüzə” Coca-Cola brendinin yeni qlobal fəlsəfəsidir. Bu fəlsəfə hər gün qarşılaşdığımız adi  hadisələrdə gizlənən möcüzələrə diqqət çəkir.“Əsl Möcüzə” hər birimizin içində yaşayır. Möcüzəni hər yerdə, həyatın özündə, insanların əməllərində və hisslərində, mütəəssir olduğu və ifadə etdiyi emosiyalarda görə, hiss edə bilərik.Birlikdə olduğumuz hər anın möcüzəyə çevrilə biləcəyini öyrədir. “Əsl Möcüzə” müasir dünyanın ziddiyyətlərini, eyni zamanda, virtual və real həyatda həmahəng olmağı öyrənən yeni nəslin kommunikasiya üsullarını qəbul etməyi öyrədir." />
    <title>Coca Cola - Əsl Möcüzə - Hekayələr</title>
    <meta property="og:title" content="Coca Cola - Əsl Möcüzə - Hekayələr" />
    <meta property="og:url" content="{{ env('APP_URL') }}" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="{{ env('APP_URL') }}/og-share.jpg" />
    <meta property="og:image:secure_url" content="{{ env('APP_URL') }}/og-share.jpg" />
    <meta property="og:image:alt" content="Coca Cola - Əsl Möcüzə - Hekayələr" />
    <meta property="og:description" content="“Əsl Möcüzə” Coca-Cola brendinin yeni qlobal fəlsəfəsidir. Bu fəlsəfə hər gün qarşılaşdığımız adi  hadisələrdə gizlənən möcüzələrə diqqət çəkir.“Əsl Möcüzə” hər birimizin içində yaşayır. Möcüzəni hər yerdə, həyatın özündə, insanların əməllərində və hisslərində, mütəəssir olduğu və ifadə etdiyi emosiyalarda görə, hiss edə bilərik.Birlikdə olduğumuz hər anın möcüzəyə çevrilə biləcəyini öyrədir. “Əsl Möcüzə” müasir dünyanın ziddiyyətlərini, eyni zamanda, virtual və real həyatda həmahəng olmağı öyrənən yeni nəslin kommunikasiya üsullarını qəbul etməyi öyrədir." />
@endsection
@section('content')
    <div class="casesWrapper">
        <div class="cases">
            @foreach(\App\Models\Story::where('confirmed',1)->orderBy('id','desc')->get() as $case)
                <div class="case">
                    <a class="popup-modal" href="#case-modal-{{$case->id}}">
                        <div class="imgWrapper">
                            <img src="/images/stories/{{$case->image}}" alt="{{$case->name}}">
                        </div>
                        <div class="caseContent">
                            <h3>{{$case->name}}</h3>
                            <p>{{$case->story}}</p>
                        </div>
                    </a>
                    <div id="case-modal-{{$case->id}}" class="mfp-hide white-popup-block">
                        <a href="#" class="popup-modal-dismiss">x</a>
                        <img src="/images/stories/{{$case->image}}" alt="{{$case->name}}">
                        <div class="white-popup-content">
                            <h2>{{$case->name}}</h2>
                            <p>{{$case->story}}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
