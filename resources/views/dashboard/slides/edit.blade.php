<x-app-layout>
    <x-slot name="header">
        <div class="row align-items-center">
            <div class="col-md-8">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    Edit Slide
                </h2>
            </div>
            <div class="col-md-4">
                <a href="{{ route('slides.index') }}" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm font-medium rounded-md text-white bg-green-400 hover:bg-green-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 float-right">
                    Slides
                </a>
            </div>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                {!! Form::open(['route' => ['slides.update',$slide->id], 'method' => 'POST','enctype'=>'multipart/form-data']) !!}
                <div class="shadow overflow-hidden sm:rounded-md">
                    <div class="px-4 py-5 bg-white sm:p-6">
                        <div class="grid grid-cols-6 gap-6">
                            <div class="col-span-12 sm:col-span-12">
                                <label for="title" class="block font-medium text-gray-700">Title</label>
                                <input type="text" name="title" value="{{$slide->title}}" id="title" autocomplete="title" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" required>
                            </div>

                            <div class="col-span-12 sm:col-span-12">
                                <label for="image" class="block font-medium text-gray-700">Image</label>
                                @if(!empty($slide->image))
                                <img src="/images/slider/{{$slide->image}}" alt="{{$slide->title}}" width="200" class="mt-1">
                                @endif
                                <input type="file" name="image" id="image" autocomplete="icon" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500">
                            </div>

                            <div class="col-span-12 sm:col-span-12">
                                <label for="video" class="block font-medium text-gray-700">Video Id(Ex: D4nCieBMJlo)</label>
                                <input type="text" name="video" value="{{$slide->video}}" id="video" autocomplete="video" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" required>
                            </div>

                            <div class="col-span-12 sm:col-span-12">
                                <label for="content" class="block font-medium text-gray-700">Content</label>
                                <textarea name="content" id="content" cols="30" rows="10" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" required>{{$slide->content}}</textarea>
                            </div>
                        </div>
                    </div>
                    {{Form::hidden('_method','PUT')}}
                    <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                        <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Save
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</x-app-layout>
