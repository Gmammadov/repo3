<x-app-layout>
    <x-slot name="header">
        <div class="row align-items-center">
            <div class="col-md-8">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    Slides
                </h2>
            </div>
            <div class="col-md-4">
                <a href="{{ route('slides.create') }}" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm font-medium rounded-md text-white bg-green-400 hover:bg-green-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 float-right">
                    Add Slide
                </a>
            </div>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="bg-white pb-4 px-4 rounded-md w-full">
                    <div class="overflow-x-auto mt-6">

                        <table class="table-auto border-collapse w-full">
                            <thead>
                            <tr class="bg-gray-200 rounded-lg text-sm font-medium text-gray-700 text-left" style="font-size: 0.9674rem">
                                <th class="px-4 py-2">Title</th>
                                <th class="px-4 py-2">Operations</th>
                            </tr>
                            </thead>
                            <tbody class="text-sm font-normal text-gray-700">
                            @foreach($slides as $slide)
                                <tr class="hover:bg-gray-100 border-b border-gray-200 py-10">
                                    <td class="px-4 py-4">{{$slide->title}}</td>
                                    <td class="px-4 py-4">
                                        <div class="flex flex-wrap">
                                            <a href="/dashboard/slides/{{$slide->id}}/edit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm font-medium rounded-md text-white bg-yellow-400 hover:bg-yellow-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 mr-2">
                                                Edit
                                            </a>
                                            {!! Form::open(['route' => ['slides.destroy', $slide->id], 'method' => 'POST', 'class' => 'inline-block']) !!}
                                            {{Form::hidden('_method', 'DELETE')}}
                                            <button onclick="return confirm('Are you sure?')" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm font-medium rounded-md text-white bg-red-600 hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                                Delete
                                            </button>
                                            {!! Form::close() !!}
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
