<x-app-layout>
    <x-slot name="header">
        <div class="row align-items-center">
            <div class="col-md-8">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    Edit Post
                </h2>
            </div>
            <div class="col-md-4">
                <a href="{{ route('posts.index') }}" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm font-medium rounded-md text-white bg-green-400 hover:bg-green-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 float-right">
                    Posts
                </a>
            </div>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                {!! Form::open(['route' => ['posts.update',$post->id], 'method' => 'POST','enctype'=>'multipart/form-data']) !!}
                <div class="shadow overflow-hidden sm:rounded-md">
                    <div class="px-4 py-5 bg-white sm:p-6">
                        <div class="grid grid-cols-6 gap-6">
                            <div class="col-span-12 sm:col-span-12">
                                <label for="title" class="block font-medium text-gray-700">Title</label>
                                <input type="text" name="title" value="{{$post->title}}" id="title" autocomplete="title" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" required>
                            </div>

                            <div class="col-span-12 sm:col-span-12">
                                <label for="image" class="block font-medium text-gray-700">Image</label>
                                @if(count($post->images) > 0)
                                <div class="row mb-4">
                                    @foreach($post->images as $image)
                                    <div class="col-md-1">
                                        <a href="/dashboard/posts/deleteImage/{{$image->id}}" onclick="return confirm('Are you sure to delete this image?')" >
                                            <img src="/images/posts/{{$image->image}}" alt="Image" class="w-full mt-1">
                                        </a>
                                    </div>
                                    @endforeach
                                </div>
                                @endif
                                <input type="file" name="image[]" id="image" autocomplete="icon" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" multiple>
                            </div>

                            <div class="col-span-12 sm:col-span-12">
                                <label for="file" class="block font-medium text-gray-700">Pdf</label>
                                <input type="file" name="file" id="image" autocomplete="icon" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" multiple>
                            </div>

                            <div class="col-span-12 sm:col-span-12">
                                <label for="author_id" class="block font-medium text-gray-700">Author</label>
                                <select name="author_id" id="author_id" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500">
                                    @foreach(\App\Models\Author::all() as $author)
                                        <option value="{{$author->id}}" @if($author->id == $post->author_id) selected @endif>{{$author->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-span-12 sm:col-span-12">
                                <label for="category_id" class="block font-medium text-gray-700">Category</label>
                                <select name="category_id" id="category_id" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500">
                                    @foreach(\App\Models\Category::all() as $category)
                                        <option value="{{$category->id}}" @if($category->id == $post->category_id) selected @endif>{{$category->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-span-12 sm:col-span-12">
                                <label for="content" class="block font-medium text-gray-700">Content</label>
                                <textarea name="content" id="content" cols="30" rows="10" class="textarea mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" required>{{$post->content}}</textarea>
                            </div>
                        </div>
                    </div>
                    {{Form::hidden('_method','PUT')}}
                    <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                        <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Save
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</x-app-layout>
