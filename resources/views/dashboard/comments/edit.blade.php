<x-app-layout>
    <x-slot name="header">
        <div class="row align-items-center">
            <div class="col-md-8">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    Edit Comment
                </h2>
            </div>
            <div class="col-md-2">
                <a href="{{ route('comments.index') }}" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm font-medium rounded-md text-white bg-green-400 hover:bg-green-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 float-right">
                    Comments
                </a>
            </div>
            <div class="col-md-2">
                <a href="/posts/{{$comment->post->slug}}" target="_blank" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm font-medium rounded-md text-white bg-green-400 hover:bg-green-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 float-right">
                    Go to the post
                </a>
            </div>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                {!! Form::open(['route' => ['comments.update',$comment->id], 'method' => 'POST']) !!}
                <div class="shadow overflow-hidden sm:rounded-md">
                    <div class="px-4 py-5 bg-white sm:p-6">
                        <div class="grid grid-cols-6 gap-6">
                            <div class="col-span-6 sm:col-span-3">
                                <label for="name" class="block font-medium text-gray-700">Name</label>
                                <input type="text" name="name" id="name" value="{{$comment->name}}" autocomplete="title" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" required>
                            </div>
                            <div class="col-span-6 sm:col-span-3">
                                <label for="email" class="block font-medium text-gray-700">Email</label>
                                <input type="email" name="email" id="email" value="{{$comment->email}}" autocomplete="email" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" required>
                            </div>
                            <div class="col-span-6 sm:col-span-3">
                                <label for="comment" class="block font-medium text-gray-700">Comment</label>
                                <input type="text" name="comment" id="comment" value="{{$comment->comment}}" autocomplete="title" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" required>
                            </div>
                            <div class="col-span-6 sm:col-span-3">
                                <label for="approved" class="block font-medium text-gray-700">Approve</label>
                                <select name="approved" id="approved" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500">
                                    <option value="1" @if($comment->approved == 1) selected @endif>Approved</option>
                                    <option value="0" @if($comment->approved == 0) selected @endif>Not approved</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    {{Form::hidden('_method','PUT')}}
                    <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                        <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Save
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</x-app-layout>
