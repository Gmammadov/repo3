<x-app-layout>
    <x-slot name="header">
        <div class="row align-items-center">
            <div class="col-md-8">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    Edit Story
                </h2>
            </div>
            <div class="col-md-4">
                <a href="{{ route('stories.index') }}" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm font-medium rounded-md text-white bg-green-400 hover:bg-green-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 float-right">
                    Stories
                </a>
            </div>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                {!! Form::open(['route' => ['stories.update',$story->id], 'method' => 'POST','enctype'=>'multipart/form-data']) !!}
                <div class="shadow overflow-hidden sm:rounded-md">
                    <div class="px-4 py-5 bg-white sm:p-6">
                        <div class="grid grid-cols-6 gap-6">
                            <div class="col-span-12 sm:col-span-12">
                                <label for="name" class="block font-medium text-gray-700">Name</label>
                                <input type="text" name="name" value="{{$story->name}}" id="name" autocomplete="title" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" required>
                            </div>

                            <div class="col-span-12 sm:col-span-12">
                                <label for="email" class="block font-medium text-gray-700">Email</label>
                                <input type="email" name="email" value="{{$story->email}}" id="title" autocomplete="title" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" required>
                            </div>

                            <div class="col-span-12 sm:col-span-12">
                                <label for="confirmed" class="block font-medium text-gray-700">Confirmed</label>
                                <select name="confirmed" id="confirmed" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500">
                                    <option value="1" @if($story->confirmed == 1) selected @endif>Confirmed</option>
                                    <option value="0" @if($story->confirmed == 0) selected @endif>Not Confirmed</option>
                                </select>
                            </div>

                            <div class="col-span-12 sm:col-span-12">
                                <label for="image" class="block font-medium text-gray-700">Image</label>
                                @if(!empty($story->image))
                                    <a href="/images/stories/{{$story->image}}" target="_blank">
                                        <img src="/images/stories/{{$story->image}}" alt="{{$story->title}}" width="200" class="mt-1">
                                    </a>
                                @endif
                                <input type="file" name="image" id="image" autocomplete="icon" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500">
                            </div>

                            <div class="col-span-12 sm:col-span-12">
                                <label for="story" class="block font-medium text-gray-700">Content</label>
                                <textarea name="story" id="story" cols="30" rows="10" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500" required>{{$story->story}}</textarea>
                            </div>
                        </div>
                    </div>
                    {{Form::hidden('_method','PUT')}}
                    <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                        <button type="submit" class="inline-flex justify-center py-2 px-4 border border-transparent shadow-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            Save
                        </button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</x-app-layout>
