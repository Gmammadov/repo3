<div class="col-md-4 col-sm-12">
    <div class="sidebar">
        <div class="widget categories-widget">
            <h3 class="widget-title">Categories</h3>
            @foreach(\App\Models\Category::all() as $category)
                <div class="category-item">
                    <a href="category.html">{{$category->title}}</a>
                    <span class="count">(4)</span>
                </div>
            @endforeach
        </div>
        <div class="widget categories-widget">
            <h3 class="widget-title">Authors</h3>
            @foreach(\App\Models\Author::all() as $author)
                <div class="category-item">
                    <a href="category.html">{{$author->title}}</a>
                    <span class="count">(4)</span>
                </div>
            @endforeach
        </div>
        <div class="widget newsletter">
            <div class="widget widget_mc4wp_form_widget">
                <h3 class="widget-title">Subscribe</h3>
                <form class="mc4wp-form mc4wp-form-101" method="post" action="#">
                    <div class="mc4wp-form-fields">
                        <label>Fill your email below to subscribe to my newsletter</label>
                        <input type="email" name="EMAIL" placeholder="Your email address" required="">
                        <input type="submit" value="Subscribe">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
