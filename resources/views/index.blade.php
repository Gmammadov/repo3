@extends('layouts.core')
@section('content')
    <!-- ========== Start Main Content ========== -->

    <section class="main-content">

        <!-- ========== Start Random Repositories ========== -->
        <div class="random-posts">
            @foreach(\App\Models\Post::orderBy('created_at','desc')->get() as $post)
            <div class="item-box">
                <div class="overlay">
                    <div class="post-thumbnail">
                        <a href="/posts/{{$post->slug}}">
                            <img class="img-responsive" src="/images/posts/{{$post->images[0]->image}}" alt="{{$post->title}}">
                        </a>
                    </div>
                    <div class="item-box-content">
                        <div class="categories">
                            <div class="post-category">
                                <ul class="post-categories">
                                    <li>
                                        <a href="category.html">{{$post->category->title}}</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <h3 class="post-title">
                            <a href="/posts/{{$post->slug}}">{{$post->title}}</a>
                        </h3>
                        <div class="author-info">
                            <span class="author-name">
                                <a href="author.html">{{$post->author->title}}</a>
                            </span>
                        </div>
                        <span class="post-date">
                            <i class="fa fa-calendar"></i>
                            <a href="archive.html">{{$post->created_at->diffForHumans()}}</a>
                        </span>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <!-- ========== End Random Repositories ========== -->

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-12">
                    <div class="row">
                        @foreach(\App\Models\Post::orderBy('created_at','desc')->get() as $post)
                        <div class="col-sm-4">
                            <div class="post-item">
                                <div class="post-thumbnail">
                                    <a href="/posts/{{$post->slug}}">
                                        <img class="img-responsive" src="/images/posts/{{$post->images[0]->image}}" alt="{{$post->title}}" >
                                    </a>
                                </div>
                                <div class="post-category">
                                    <ul class="post-categories">
                                        <li>
                                            <a href="/categories/{{$post->category->id}}">{{$post->category->title}}</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="post-header">
                                    <h3 class="post-title">
                                        <a href="/posts/{{$post->slug}}">{{$post->title}}</a>
                                    </h3>
                                    <span class="post-date">
                                        <i class="fa fa-calendar"></i>
                                        <span>{{$post->created_at->diffForHumans()}}</span>
                                    </span>
                                </div>
                                <div class="post-content">
                                    <p>{!! Str::words(strip_tags($post->content), '15') !!}</p>
                                </div>
                                <div class="post-footer">
                                    <div class="author-info pull-left">
                                        <span class="author-name">
                                            <a href="/authors/{{$post->author->id}}">{{$post->author->title}}</a>
                                        </span>
                                    </div>
                                    <div class="read-more pull-right">
                                        <a href="/posts/{{$post->slug}}">Continue Reading<i class="fa fa-angle-double-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="clearfix"></div>
{{--                    <div class="pagination-numbers">--}}
{{--                        <span aria-current="page" class="page-numbers current">1</span>--}}
{{--                        <a class="page-numbers" href="#">2</a>--}}
{{--                        <a class="page-numbers" href="#">3</a>--}}
{{--                        <a class="page-numbers" href="#">4</a>--}}
{{--                        <a class="next page-numbers" href="#">--}}
{{--                            <span class="fa fa-angle-right"></span>--}}
{{--                        </a>--}}
{{--                    </div>--}}
                </div>

                @include('sidebar')

            </div>
        </div>
    </section>

    <!-- ========== End Main Content ========== -->
@endsection
