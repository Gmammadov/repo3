@extends('layouts.core')
@section('meta')
    <meta name="description" content="“Əsl Möcüzə” Coca-Cola brendinin yeni qlobal fəlsəfəsidir. Bu fəlsəfə hər gün qarşılaşdığımız adi  hadisələrdə gizlənən möcüzələrə diqqət çəkir.“Əsl Möcüzə” hər birimizin içində yaşayır. Möcüzəni hər yerdə, həyatın özündə, insanların əməllərində və hisslərində, mütəəssir olduğu və ifadə etdiyi emosiyalarda görə, hiss edə bilərik.Birlikdə olduğumuz hər anın möcüzəyə çevrilə biləcəyini öyrədir. “Əsl Möcüzə” müasir dünyanın ziddiyyətlərini, eyni zamanda, virtual və real həyatda həmahəng olmağı öyrənən yeni nəslin kommunikasiya üsullarını qəbul etməyi öyrədir." />
    <title>Coca Cola - Əsl Möcüzə - Bloq</title>
    <meta property="og:title" content="Coca Cola - Əsl Möcüzə - Bloq" />
    <meta property="og:url" content="{{ env('APP_URL') }}/posts" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="{{ env('APP_URL') }}/og.jpg" />
    <meta property="og:image:secure_url" content="{{ env('APP_URL') }}/og.jpg" />
    <meta property="og:image:alt" content="Coca Cola - Əsl Möcüzə - Bloq" />
    <meta name="description" content="“Əsl Möcüzə” Coca-Cola brendinin yeni qlobal fəlsəfəsidir. Bu fəlsəfə hər gün qarşılaşdığımız adi  hadisələrdə gizlənən möcüzələrə diqqət çəkir.“Əsl Möcüzə” hər birimizin içində yaşayır. Möcüzəni hər yerdə, həyatın özündə, insanların əməllərində və hisslərində, mütəəssir olduğu və ifadə etdiyi emosiyalarda görə, hiss edə bilərik.Birlikdə olduğumuz hər anın möcüzəyə çevrilə biləcəyini öyrədir. “Əsl Möcüzə” müasir dünyanın ziddiyyətlərini, eyni zamanda, virtual və real həyatda həmahəng olmağı öyrənən yeni nəslin kommunikasiya üsullarını qəbul etməyi öyrədir." />
@endsection
@section('content')
<div class="posts">
@foreach(\App\Models\Post::orderBy('created_at','desc')->get() as $post)
    <div class="fullWidthSection">
        <div class="column">
            <div class="newsContent">
                <h2>{{$post->title}}</h2>
                <p>{!! Str::words(strip_tags($post->content), '30') !!}</p>
                <div class="buttonWrapper">
                    <a href="/posts/{{$post->slug}}" class="button">Ətraflı Oxu</a>
                </div>
            </div>
        </div>
        <div class="column image">
            <img src="/images/posts/{{$post->images[0]->image}}" alt="{{$post->title}}">
        </div>
    </div>
@endforeach
</div>
@endsection
