<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Submission;

class SubmissionsController extends Controller
{

    public function index()
    {
        $submissions =  Submission::all();
        return view('dashboard.submissions.index')->with("submissions",$submissions);
    }


    public function edit($id)
    {
        $submission = Submission::find($id);
        return view('dashboard.submissions.edit')->with('submission', $submission);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'message' => 'required',
            'approved' => 'required',
        ]);

        $submission = Submission::find($id);
        $submission->name = $request->input('name');
        $submission->email = $request->input('email');
        $submission->message = $request->input('message');
        $submission->approved = $request->input('approved');
        $submission->save();

        return redirect('/dashboard/submissions/')->with('success', 'Submission updated!');
    }

    public function destroy($id)
    {
        $submission = Submission::find($id);
        $submission->delete();
        return redirect('/dashboard/submissions/')->with('success', 'Submission Removed!');
    }
}
