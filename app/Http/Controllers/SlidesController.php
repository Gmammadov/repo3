<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Slide;
use Illuminate\Support\Str;
use Image;

class SlidesController extends Controller
{

    public function index()
    {
        $slides =  Slide::all();
        return view('dashboard.slides.index')->with("slides",$slides);
    }

    public function create()
    {
        return view('dashboard.slides.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'image' => 'required|max:5120|image',
        ]);

        $slide = new Slide;
        $slide->title = $request->input('title');
        $slide->content = $request->input('content');
        $slide->video = $request->input('video');

        if($request->hasFile('image')){
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $filename = Str::slug($filename, '-');
            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            $image_original = Image::make($request->file('image')->getRealPath());
            $image_original->backup();
            $image_original->save(public_path('images/slider/'.$fileNameToStore));
        }else{
            $fileNameToStore = 'slide.jpg';
        }


        $slide->image = $fileNameToStore;
        $slide->save();

        return redirect('/dashboard/slides/')->with('success', 'Slide created!');
    }


    public function edit($id)
    {
        $slide = Slide::find($id);
        return view('dashboard.slides.edit')->with('slide', $slide);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'image' => 'max:5120|image',
        ]);

        $slide = Slide::find($id);
        $slide->title = $request->input('title');
        $slide->content = $request->input('content');
        $slide->video = $request->input('video');
        if($request->hasFile('image')){
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $filename = Str::slug($filename, '-');
            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            $image_original = Image::make($request->file('image')->getRealPath());
            $image_original->backup();
            $image_original->save(public_path('images/slider/'.$fileNameToStore));

            $slide->image = $fileNameToStore;
        }
        $slide->save();

        return redirect('/dashboard/slides/')->with('success', 'Slide updated!');
    }

    public function destroy($id)
    {
        $slide = Slide::find($id);
        $slide->delete();
        return redirect('/dashboard/slides/')->with('success', 'Slide Removed!');
    }
}
