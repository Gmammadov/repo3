<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentsController extends Controller
{

    public function index()
    {
        $comments =  Comment::all();
        return view('dashboard.comments.index')->with("comments",$comments);
    }


    public function edit($id)
    {
        $comment = Comment::find($id);
        return view('dashboard.comments.edit')->with('comment', $comment);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'comment' => 'required',
            'approved' => 'required',
        ]);

        $comment = Comment::find($id);
        $comment->name = $request->input('name');
        $comment->email = $request->input('email');
        $comment->comment = $request->input('comment');
        $comment->approved = $request->input('approved');
        $comment->save();

        return redirect('/dashboard/comments/')->with('success', 'Comment updated!');
    }

    public function destroy($id)
    {
        $comment = Comment::find($id);
        $comment->delete();
        return redirect('/dashboard/comments/')->with('success', 'Comment Removed!');
    }
}
