<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoriesController extends Controller
{

    public function index()
    {
        $categories =  Category::all();
        return view('dashboard.categories.index')->with("categories",$categories);
    }

    public function create()
    {
        return view('dashboard.categories.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);

        $category = new Category;
        $category->title = $request->input('title');
        $category->save();

        return redirect('/dashboard/categories/')->with('success', 'Category created!');
    }


    public function edit($id)
    {
        $category = Category::find($id);
        return view('dashboard.categories.edit')->with('category', $category);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);

        $category = Category::find($id);
        $category->title = $request->input('title');
        $category->save();

        return redirect('/dashboard/categories/')->with('success', 'Category updated!');
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        return redirect('/dashboard/categories/')->with('success', 'Category Removed!');
    }
}
