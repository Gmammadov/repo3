<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Social;

class SocialsController extends Controller
{

    public function index()
    {
        $socials =  Social::all();
        return view('dashboard.socials.index')->with("socials",$socials);
    }

    public function create()
    {
        return view('dashboard.socials.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'icon' => 'required',
            'link' => 'required'
        ]);

        $social = new Social;
        $social->title = $request->input('title');
        $social->icon = $request->input('icon');
        $social->link = $request->input('link');
        $social->save();

        return redirect('/dashboard/socials/')->with('success', 'Social created!');
    }


    public function edit($id)
    {
        $social = Social::find($id);
        return view('dashboard.socials.edit')->with('social', $social);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'icon' => 'required',
            'link' => 'required'
        ]);

        $social = Social::find($id);
        $social->title = $request->input('title');
        $social->icon = $request->input('icon');
        $social->link = $request->input('link');
        $social->save();

        return redirect('/dashboard/socials/')->with('success', 'Social updated!');
    }

    public function destroy($id)
    {
        $social = Social::find($id);
        $social->delete();
        return redirect('/dashboard/socials/')->with('success', 'Social Removed!');
    }
}
