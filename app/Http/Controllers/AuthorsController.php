<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Author;

class AuthorsController extends Controller
{

    public function index()
    {
        $authors =  Author::all();
        return view('dashboard.authors.index')->with("authors",$authors);
    }

    public function create()
    {
        return view('dashboard.authors.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);

        $author = new Author;
        $author->title = $request->input('title');
        $author->save();

        return redirect('/dashboard/authors/')->with('success', 'Author created!');
    }


    public function edit($id)
    {
        $author = Author::find($id);
        return view('dashboard.authors.edit')->with('author', $author);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required'
        ]);

        $author = Author::find($id);
        $author->title = $request->input('title');
        $author->save();

        return redirect('/dashboard/authors/')->with('success', 'Author updated!');
    }

    public function destroy($id)
    {
        $author = Author::find($id);
        $author->delete();
        return redirect('/dashboard/authors/')->with('success', 'Author Removed!');
    }
}
