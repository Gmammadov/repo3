<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use App\Models\Story;
use App\Models\Submission;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Image;

class PagesController extends Controller
{
    public function index() {
        return view('index');
    }

    public function about() {
        return view('about');
    }

    public function contact() {
        return view('contact');
    }

    public function posts() {
        return view('posts');
    }

    public function post($slug) {
        $post = Post::where('slug', $slug)->first();
        return view('post', compact('post'));
    }

    public function search($search) {
        $posts = Post::where('title', 'LIKE', '%' . $search . '%')->orWhere('content', 'LIKE', '%' . $search . '%')->get();
        return view('search', compact('posts'));
    }

    public function comment(Request $request, $post_id) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'comment' => 'required',
        ]);

        $comment = new Comment();
        $comment->name = $request->input('name');
        $comment->email = $request->input('email');
        $comment->comment = $request->input('comment');
        $comment->post_id = $post_id;
        $comment->save();

        return back()->with('success', 'Comment submitted! We will review and approve your comment as soon as possible!');
    }

    public function submit(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'message' => 'required',
            'file' => 'required',
        ]);

        $submission = new Submission();
        $submission->name = $request->input('name');
        $submission->email = $request->input('email');
        $submission->message = $request->input('message');
        $submission->subject = $request->input('subject');
        $submission->approved = 0;

        if($request->hasFile('file')){
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $fileNameToStore = $filename.'.'.$extension;
            $path = $request->file('file')->storeAs('submissions', $fileNameToStore);

            $submission->file = $fileNameToStore;
        }

        $submission->save();

        return back()->with('success', 'We have received your submission! We will review and let you know as soon as possible');
    }

}
