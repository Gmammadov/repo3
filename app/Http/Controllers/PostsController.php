<?php

namespace App\Http\Controllers;

use App\Models\PostImage;
use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Str;
use Image;

class PostsController extends Controller
{

    private function uniqueSlug($title) {
        $slug = Str::slug($title,'-');
        if(Post::where('slug',$slug)->exists()){
            $count = Post::where('slug','LIKE',"{$slug}%")->count();
            $newCount = $count > 0 ? ++$count : '';
            return $newCount > 0 ? "$slug-$newCount" : $slug;
        }else{
            return $slug;
        }
    }

    public function index()
    {
        $posts =  Post::all();
        return view('dashboard.posts.index')->with("posts",$posts);
    }

    public function create()
    {
        return view('dashboard.posts.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
        ]);

        $post = new Post;
        $post->title = $request->input('title');
        $post->content = $request->input('content');
        $post->category_id = $request->input('category_id');
        $post->author_id = $request->input('author_id');
        $post->slug = $this->uniqueSlug($request->input("title"));

        if($request->hasFile('file')){
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $fileNameToStore = $filename.'.'.$extension;
            $path = $request->file('file')->storeAs('posts/pdf', $fileNameToStore);
        }else{
            $fileNameToStore = 'nopdf.pdf';
        }

        $post->file = $fileNameToStore;

        $post->save();

        foreach ($request->file('image') as $image) {
            $filenameWithExt = $image->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $image->getClientOriginalExtension();
            $filename = Str::slug($filename, '-');
            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            $image_original = Image::make($image->getRealPath());
            $image_original->backup();
            $image_original->save(public_path('images/posts/'.$fileNameToStore));

            $postImage = new PostImage;
            $postImage->image = $fileNameToStore;
            $postImage->post_id = $post->id;
            $postImage->save();
        }

        return redirect('/dashboard/posts/')->with('success', 'Post created!');
    }


    public function edit($id)
    {
        $post = Post::find($id);
        return view('dashboard.posts.edit')->with('post', $post);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
        ]);

        $post = Post::find($id);
        $post->title = $request->input('title');
        $post->content = $request->input('content');
        $post->category_id = $request->input('category_id');
        $post->author_id = $request->input('author_id');

        if($request->file('image')) {
            foreach ($request->file('image') as $image) {
                $filenameWithExt = $image->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $image->getClientOriginalExtension();
                $filename = Str::slug($filename, '-');
                $fileNameToStore = $filename.'_'.time().'.'.$extension;

                $image_original = Image::make($image->getRealPath());
                $image_original->backup();
                $image_original->save(public_path('images/posts/'.$fileNameToStore));

                $image_300 = Image::make($image->getRealPath());
                $image_300->fit(480, 640);
                $image_300->save(public_path('images/posts/480x640/'.$fileNameToStore));

                $postImage = new PostImage;
                $postImage->image = $fileNameToStore;
                $postImage->post_id = $id;
                $postImage->save();
            }
        }



        if($request->hasFile('file')){
            $filenameWithExt = $request->file('file')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $fileNameToStore = $filename.'.'.$extension;
            $path = $request->file('file')->storeAs('posts/pdf', $fileNameToStore);

            $post->file = $fileNameToStore;
        }

        $post->save();

        return redirect('/dashboard/posts/')->with('success', 'Post updated!');
    }

    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();
        return redirect('/dashboard/posts/')->with('success', 'Post Removed!');
    }

    public function deleteImage($id)
    {
        $post = PostImage::find($id);
        $post->delete();
        return back()->with('success', 'Image Removed!');
    }
}
