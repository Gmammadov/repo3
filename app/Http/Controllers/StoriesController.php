<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Story;
use Illuminate\Support\Str;
use Image;

class StoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stories =  Story::all();
        return view('dashboard.stories.index')->with("stories",$stories);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $story = Story::find($id);
        return view('dashboard.stories.edit')->with('story', $story);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'story' => 'required',
            'email' => 'required',
            'image' => 'max:5120|image',
        ]);

        $story = Story::find($id);
        $story->name = $request->input('name');
        $story->email = $request->input('email');
        $story->story = $request->input('story');
        $story->confirmed = $request->input('confirmed');
        if($request->hasFile('image')){
            $filenameWithExt = $request->file('image')->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $filename = Str::slug($filename, '-');
            $fileNameToStore = $filename.'_'.time().'.'.$extension;

            $image_original = Image::make($request->file('image')->getRealPath());
            $image_original->backup();
            $image_original->save(public_path('images/stories/'.$fileNameToStore));

            $story->image = $fileNameToStore;
        }
        $story->save();

        return redirect('/dashboard/stories/')->with('success', 'Story updated!');
    }


    public function destroy($id)
    {
        $story = Story::find($id);
        $story->delete();
        return redirect('/dashboard/stories/')->with('success', 'Story Removed!');
    }
}
