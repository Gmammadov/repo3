<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    public function images()
    {
        return $this->hasMany('App\Models\PostImage');
    }

    public function author()
    {
        return $this->belongsTo('App\Models\Author');
    }

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function comments()
    {
        return $this->hasMany('App\Models\Comment','post_id', 'id')->where('approved',1);
    }
}
