$(document).ready(function(){
    $('.slider').slick({
        dots: true,
        appendDots: '.sliderDots',
        prevArrow: $('.sliderNav .prev'),
        nextArrow: $('.sliderNav .next'),
    });

    $('.newsSlider').slick({
        dots: true,
        appendDots: '.newsSliderDots',
        prevArrow: $('.newSliderNav .prev'),
        nextArrow: $('.newSliderNav .next'),
    });
});
