-- -------------------------------------------------------------
-- TablePlus 3.9.1(342)
--
-- https://tableplus.com/
--
-- Database: repository
-- Generation Time: 2022-04-23 23:00:24.1250
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


DROP TABLE IF EXISTS `authors`;
CREATE TABLE `authors` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `post_id` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `post_images`;
CREATE TABLE `post_images` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `file` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `author_id` int NOT NULL,
  `category_id` int NOT NULL,
  `approved` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `slides`;
CREATE TABLE `slides` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `video` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `socials`;
CREATE TABLE `socials` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `stories`;
CREATE TABLE `stories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `story` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `confirmed` int NOT NULL DEFAULT '0',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `submissions`;
CREATE TABLE `submissions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `approved` int NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `authors` (`id`, `title`, `created_at`, `updated_at`) VALUES
('2', 'Isabel John', '2022-01-22 23:23:16', '2022-01-22 23:23:16'),
('3', 'Amina Souag', '2022-01-22 23:25:14', '2022-01-22 23:25:14'),
('4', 'Ambrosio Toval', '2022-01-22 23:28:14', '2022-01-22 23:28:14'),
('5', 'Ronald Houde', '2022-01-24 17:45:09', '2022-01-24 17:45:09'),
('6', 'Andreas Fuchs and Roland Rieke', '2022-01-24 17:49:33', '2022-01-24 17:49:33'),
('7', 'Massila Kamalrudin, Sabrina Ahmad, Naveed Ikram', '2022-01-24 17:53:55', '2022-01-24 17:53:55'),
('8', 'W. Lam, J. A. McDermid & A. J. Vickers', '2022-01-24 18:06:23', '2022-01-24 18:06:23');

INSERT INTO `categories` (`id`, `title`, `created_at`, `updated_at`) VALUES
('7', 'Requirements Engineering', '2022-01-22 23:16:03', '2022-01-22 23:25:22'),
('8', 'Elicitation of Requirements', '2022-01-22 23:22:38', '2022-01-22 23:22:38'),
('9', 'Requirements Reuse', '2022-01-22 23:27:58', '2022-01-22 23:27:58'),
('11', 'Deployment Packages and Case Study', '2022-01-24 17:44:59', '2022-01-24 17:44:59'),
('13', 'Identification of Security Requirements', '2022-01-24 17:49:17', '2022-01-24 17:49:17');

INSERT INTO `comments` (`id`, `name`, `email`, `comment`, `approved`, `created_at`, `updated_at`, `post_id`) VALUES
('1', 'John Doe', 'arif.mmdv@gmail.com', 'Thank you for the website', '1', '2022-04-17 16:01:19', '2022-04-19 20:58:14', '14'),
('2', 'Simar Talob', 'qasim@gmail.com', 'I liked this post', '1', '2022-04-17 16:25:04', '2022-04-19 20:59:18', '14');

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
('1', '2014_10_12_000000_create_users_table', '1'),
('2', '2014_10_12_100000_create_password_resets_table', '1'),
('3', '2019_08_19_000000_create_failed_jobs_table', '1'),
('4', '2019_12_14_000001_create_personal_access_tokens_table', '1'),
('5', '2021_11_28_153641_create_socials_table', '2'),
('6', '2021_11_28_154712_create_stories_table', '3'),
('7', '2021_11_28_160428_add_column_to_stories_table', '4'),
('8', '2021_12_07_180054_create_slides_table', '5'),
('9', '2021_12_07_221803_create_posts_table', '6'),
('10', '2021_12_07_221811_create_post_images_table', '6'),
('11', '2021_12_16_013355_add_video_to_slides_table', '7'),
('12', '2022_01_18_231022_create_categories_table', '8'),
('13', '2022_01_18_232521_create_authors_table', '9'),
('14', '2022_01_18_233532_add_columns_to_posts_table', '10'),
('15', '2022_04_17_154518_create_comments_table', '11'),
('16', '2022_04_17_155454_add_column_to_comments_tabl', '12'),
('17', '2022_04_17_162921_add_column_to_posts_table', '13'),
('18', '2022_04_17_163507_create_submissions_table', '14');

INSERT INTO `post_images` (`id`, `post_id`, `image`, `created_at`, `updated_at`) VALUES
('1', '2', 'w-1616301368801373410814-1000x669_1638916748.jpg', '2021-12-08 00:39:09', '2021-12-08 00:39:09'),
('2', '2', 'w-1616301368801747130277-1000x669_1638916749.jpeg', '2021-12-08 00:39:09', '2021-12-08 00:39:09'),
('3', '2', 'w-1616301368802104349220-1000x669_1638916749.jpeg', '2021-12-08 00:39:09', '2021-12-08 00:39:09'),
('4', '2', 'w-1616301368803170146281-1000x669_1638916749.jpeg', '2021-12-08 00:39:09', '2021-12-08 00:39:09'),
('5', '2', 'w-1616301368803171117682-1000x669_1638916749.jpg', '2021-12-08 00:39:09', '2021-12-08 00:39:09'),
('6', '2', 'w-1616301368803498228739-1000x669_1638916749.jpg', '2021-12-08 00:39:09', '2021-12-08 00:39:09'),
('7', '2', 'w-1616301368803743916480-1000x669_1638916749.jpeg', '2021-12-08 00:39:09', '2021-12-08 00:39:09'),
('8', '3', 'post_1638917186.jpeg', '2021-12-08 00:46:27', '2021-12-08 00:46:27'),
('9', '4', 'a4f6d52b-3f5e-324d-9d34-4592f2a0fca0-850_1638917288.jpeg', '2021-12-08 00:48:08', '2021-12-08 00:48:08'),
('10', '4', '3b18b405-d6fe-34cc-ae81-edd09d111f30_1638917288.jpeg', '2021-12-08 00:48:08', '2021-12-08 00:48:08'),
('11', '4', '07a4b0bf-24d7-341e-833d-220c20ca1ad3_1638917288.jpeg', '2021-12-08 00:48:08', '2021-12-08 00:48:08'),
('12', '4', '178ee8dd-0156-39e3-8694-d7f82ac1f8bf_1638917288.jpeg', '2021-12-08 00:48:08', '2021-12-08 00:48:08'),
('13', '4', 'ae8d2db1-a33c-3a23-8caf-2468f0f1f825_1638917288.jpeg', '2021-12-08 00:48:08', '2021-12-08 00:48:08'),
('14', '4', 'd984f50d-e113-3e16-a496-023599849afc_1638917288.jpeg', '2021-12-08 00:48:09', '2021-12-08 00:48:09'),
('15', '5', '1615197869045-dfdzgvan_1638917358.jpeg', '2021-12-08 00:49:18', '2021-12-08 00:49:18'),
('16', '5', '1615197860553-k1kbe8ao_1638917401.jpeg', '2021-12-08 00:50:01', '2021-12-08 00:50:01'),
('17', '5', '1615197861368-sz6bg5as_1638917401.jpeg', '2021-12-08 00:50:01', '2021-12-08 00:50:01'),
('18', '5', '1615197862707-lp9pnom5_1638917401.jpeg', '2021-12-08 00:50:01', '2021-12-08 00:50:01'),
('19', '5', '1615197863139-sjy2vsoc_1638917401.jpeg', '2021-12-08 00:50:01', '2021-12-08 00:50:01'),
('20', '5', '1615197863740-kqyoz4ne_1638917401.jpeg', '2021-12-08 00:50:01', '2021-12-08 00:50:01'),
('21', '5', '1615197864572-9ldjnan5_1638917401.jpeg', '2021-12-08 00:50:01', '2021-12-08 00:50:01'),
('22', '6', 'untitled-design-11-2000x570_1642625458.png', '2022-01-19 22:50:58', '2022-01-19 22:50:58'),
('23', '7', 'screenshot-2022-01-19-at-230359_1642626281.png', '2022-01-19 23:04:41', '2022-01-19 23:04:41'),
('24', '8', '81o8tnwuj-l_1642866657.jpeg', '2022-01-22 17:50:57', '2022-01-22 17:50:57'),
('25', '9', '81o8tnwuj-l_1642866781.jpeg', '2022-01-22 17:53:01', '2022-01-22 17:53:01'),
('26', '10', '81o8tnwuj-l_1642866864.jpeg', '2022-01-22 17:54:24', '2022-01-22 17:54:24'),
('27', '11', 'logo1_1642885613.png', '2022-01-22 23:06:53', '2022-01-22 23:06:53'),
('28', '12', '1519901846615_1642886270.jpeg', '2022-01-22 23:17:50', '2022-01-22 23:17:50'),
('29', '13', '1519901846615_1642886654.jpeg', '2022-01-22 23:24:14', '2022-01-22 23:24:14'),
('30', '14', '1519901846615_1642886831.jpeg', '2022-01-22 23:27:11', '2022-01-22 23:27:11'),
('31', '15', '1519901846615_1642887008.jpeg', '2022-01-22 23:30:08', '2022-01-22 23:30:08'),
('32', '16', '1519901846615_1643039256.jpeg', '2022-01-24 17:47:37', '2022-01-24 17:47:37'),
('33', '17', '1519901846615_1643039261.jpeg', '2022-01-24 17:47:42', '2022-01-24 17:47:42'),
('34', '18', '1519901846615_1643039460.jpeg', '2022-01-24 17:51:00', '2022-01-24 17:51:00'),
('35', '19', 'screen-shot-2022-01-24-at-175417_1643039952.png', '2022-01-24 17:59:13', '2022-01-24 17:59:13'),
('36', '19', '1519901846615_1643040006.jpeg', '2022-01-24 18:00:06', '2022-01-24 18:00:06'),
('37', '20', '1519901846615_1643040088.jpeg', '2022-01-24 18:01:28', '2022-01-24 18:01:28'),
('38', '21', '1519901846615_1643040488.jpeg', '2022-01-24 18:08:08', '2022-01-24 18:08:08'),
('39', '13', 'what-is-cyber-security_1650401637.jpeg', '2022-04-19 20:53:57', '2022-04-19 20:53:57'),
('40', '13', 'bigstock-internet-security-firewall-or-326464240-1024x684_1650401637.png', '2022-04-19 20:53:58', '2022-04-19 20:53:58'),
('41', '13', 'cybersecurity-04-13_1650401638.png', '2022-04-19 20:53:59', '2022-04-19 20:53:59');

INSERT INTO `posts` (`id`, `title`, `slug`, `content`, `deleted_at`, `created_at`, `updated_at`, `file`, `author_id`, `category_id`, `approved`) VALUES
('13', 'Elicitation of Requirements from User Documentation', 'elicitation-of-requirements-from-user-documentation', '<p>This paper describes an approach for elicitation of requirements based on existing user documentation. The approach we describe in this paper supports capturing of the information found in user documentation of legacy systems, e.g., user manuals, and the specification of this information in requirements specifications, using, e.g., Use Cases. We propose a conceptual model describing the transition from user documentation to requirements artefacts describing common and variable elements of a product line model or requirements specification. We present heuristics that allow easy identification of text elements in user documents that are then used to create a significant part of the requirements specification and product line model, respectively.</p>', NULL, '2022-01-22 23:24:14', '2022-01-22 23:24:38', '10.1.1.79.7047.pdf', '2', '8', '0'),
('14', 'Reusable knowledge in security requirements engineering: a systematic mapping study', 'reusable-knowledge-in-security-requirements-engineering-a-systematic-mapping-study', '<p>Security is a concern that must be taken into consideration starting from the early stages of system development. Over the last two decades, researchers and engineers have developed a considerable number of methods for security requirements engineering. Some of them rely on the (re) use of security knowledge. Despite some existing surveys about security requirements engineering, there is not yet any reference for researchers and practitioners that presents in a systematic way the existing proposals, techniques, and tools related to security knowledge reuse in security requirements engineering. The aim of this paper is to fill this gap by looking into drawing a picture of the literature on knowledge and reuse in security requirements engineering. The questions we address are related to methods, techniques, modelling frameworks, and tools for and by reuse in security requirements engineering. We address these questions through a systematic mapping study. The mapping study was a literature review conducted with the goal of identifying, analyzing and categorizing state of the art research on our topic. This mapping study analyzes more than thirty approaches, covering twenty years of research in security requirements engineering. The contributions can be summarized as follows: (i) a framework was defined for analyzing and comparing the different proposals as well as categorizing future contributions related to knowledge reuse and security requirements engineering; (ii) the different forms of knowledge representation and reuse were identified; and (iii) previous surveys were updated. We conclude that most methods should introduce more reusable knowledge to manage security requirements.</p>', NULL, '2022-01-22 23:27:11', '2022-01-22 23:27:11', 'Reusableknowledgeinsecurityrequirementsengineering-asystematicmappingstudy_REJournal_2015.pdf', '3', '7', '0'),
('15', 'Requirements Reuse for Improving Information Systems Security: A Practitioner’s Approach', 'requirements-reuse-for-improving-information-systems-security-a-practitioners-approach', '<p>Information systems security issues have usually been considered only after the system has been developed completely, and rarely during its design, coding, testing or deployment. However, the advisability of considering security from the very beginning of the system development has recently begun to be appreciated, in particular in the system requirements specification phase. We present a practical method to elicit and specify the system and software requirements, including a repository containing reusable requirements, a spiral process model, and a set of requirements documents templates. In this paper, this method is focused on the security of information systems and, thus, the reusable requirements repository contains all the requirements taken from MAGERIT, the Spanish public administration risk analysis and management method, which conforms to ISO 15408, Common Criteria Framework. Any information system including these security requirements must therefore pass a risk analysis and management study performed with MAGERIT. The requirements specification templates are hierarchically structured and are based on IEEE standards. Finally, we show a case study in a system of our regional administration aimed at managing state subsidies.</p>\r\n\r\n<p>Keywords: Common criteria framework; Requirements engineering; Requirements reuse; Risk analysis and management methods; Security</p>', NULL, '2022-01-22 23:30:08', '2022-01-22 23:30:08', 'toval2002.pdf', '4', '9', '0'),
('16', 'Deployment Packages and Case Study for Systems Engineering', 'deployment-packages-and-case-study-for-systems-engineering', '<p>Very small entities (VSEs) play an increasingly important role in the global economy. The products they develop are often integrated into products made by larger enterprises. Clients, furthermore, demand of the VSEs that they assume a much broader role, spanning the entire development life-cycle of the product instead of being limited to a &ldquo;build-to-print&rdquo; approach. The ISO/IEC 29110 systems engineering management and engineering guides were developed mainly from ISO/IEC/IEEE 15288 to address this new reality, to exploit the lean and efficient nature of VSEs and to adapt to their typical budget and resource constraints. By design, the management and engineering guide is supported by Deployment Packages (DP), the development of which was taken on by the INCOSE VSE Working Group. A DP is a set of artefacts designed to facilitate the implementation of the management and engineering guides of ISO/IEC 29110 by VSEs. In tune with the need for low cost and flexibility, Open Source software tools are emerging to support VSEs and provide a bridge with &ldquo;Big League&rdquo; development life-cycle toolsets. Finally, to make the deployment of ISO/IEC 29110 possible in VSEs, training packages, supported by relevant pilot projects help VSE personnel learn how to apply all of the above. This paper describes the Systems Engineering DP for Requirements Engineering (RE DP) and shows how it can be applied using the Autonomous Rover Case Study developed under the Eclipse Foundation Polarsys project.</p>', NULL, '2022-01-24 17:47:36', '2022-01-24 17:47:36', 'Deployment Packages and Case Study for Systems Engineering.pdf', '5', '11', '0'),
('18', 'Identification of Security Requirements in Systems of Systems by Functional Security Analysis', 'identification-of-security-requirements-in-systems-of-systems-by-functional-security-analysis', '<p>Cooperating systems typically base decisions on information from their own components as well as on input from other systems. Safety-critical&nbsp;decisions based on cooperative reasoning however raise severe concerns to security issues. Here, we address the security requirements elicitation step in the security engineering process for such systems of systems. The method comprises the tracing down of functional dependencies over system component boundaries right onto the origin of information as a functional flow graph. Based on this graph, we systematically deduce comprehensive sets of formally defined authenticity requirements for the given security and dependability objectives. The proposed method thereby avoids premature assumptions on the security architecture&rsquo;s structure as well as the means by which it is realised. Furthermore, a tool-assisted approach that follows the presented methodology is described.</p>\r\n\r\n<p>Keywords: security requirements elicitation, systems of systems security engineering, security analysis for vehicular communication systems.</p>', NULL, '2022-01-24 17:51:00', '2022-01-24 17:51:00', 'Identification of Security Requirements in Systems of Systems by Functional Security Analysis.pdf', '6', '13', '0'),
('20', 'Requirements Engineering for Internet of Things', 'requirements-engineering-for-internet-of-things', '<p style=\"text-align:justify\">Requirements Engineering (RE) is now a well-established discipline of research and practice in software and systems development. The importance of developing and following effective RE practices has long been recognized by researchers and practitioners alike. In the Asia&ndash;Pacific region, RE is also receiving more and more attention due to the increasing reliance on software-intensive systems.&nbsp;The Asia-Pacific Requirements Engineering Symposium (APRES) is a focused&nbsp;intellectual forum for in-depth discussion of all issues related to RE. It aims to bring&nbsp;together researchers and practitioners from industry, academia, and government to&nbsp;share the state of the art and practice in RE and explore emerging challenges in RE&nbsp;innovation. It also aims to foster collaboration among the RE community of researchers&nbsp;and practitioners in Asia and Oceania. Following the success of APRES 2014 in New&nbsp;Zealand, APRES 2015 in China, and APRES 2016 in Japan, the fourth edition of&nbsp;APRES (APRES 2017) took place in Malaysia, specifically in Melaka the historical&nbsp;city of Malaysia.&nbsp;Responding to the growing trend toward interconnectivity, the discussion at APRES&nbsp;2017 addressed the changing work practices and challenges of RE in response to the&nbsp;Internet of Things (IoT). This year, we accepted 11 full papers and five short papers.&nbsp;All papers were carefully reviewed by at least three Program Committee members and&nbsp;detailed constructive feedback was provided to the authors. We also had participants&nbsp;and presentations from Australia, Malaysia, Pakistan, Japan and Korea.&nbsp;APRES 2017 was held for two days during November 9&ndash;10, 2017. The symposium&nbsp;hosted two keynote speakers who each presented a topic relevant to the current&nbsp;development of RE. The first speaker, Professor Didar Zowghi from Australia, highlighted&nbsp;the sociotechnical perspectives in RE, while the second speaker, Professor&nbsp;Madhusudan Singh from Korea, discussed Blockchain Technology in RE. Both topics&nbsp;are relevant to the changing work practice and research in RE.&nbsp;The paper presentations at APRES 2017 were organized into five main topics, i.e.,&nbsp;big data, cyber security, crowd-sourcing, automation, and requirements challenges.&nbsp;APRES 2017 also attracted a good number of participants and enriched the overall&nbsp;offering of the conference for the discussions of real-world problems and sharing of&nbsp;industry experiences and practices in RE.&nbsp;Our greatest thanks go to the authors and presenters whose contributions made&nbsp;APRES 2017 a success. We are grateful to the Program Committee members for their&nbsp;thorough and timely reviews of the submissions. We thank the Steering Committee for&nbsp;their valuable guidance.&nbsp;Our thanks also go to Springer, publisher of the APRES proceedings, for their&nbsp;continuous support. Finally, thanks to EasyChair for making the conference management&nbsp;such an efficient task.</p>\r\n\r\n<p style=\"text-align:justify\">We hope you all enjoy the APRES 2017 proceedings.</p>', NULL, '2022-01-24 18:01:28', '2022-01-24 18:01:28', 'Requirements Engineering for Internet of Things 4th Asia-Pacific Symposium, APRES 2017, Melaka, Malaysia, November 9–10, 2017, Proceedings by Massila Kamalrudin,Sabrina Ahmad,Naveed Ikram (eds.) (z-lib.org).pdf', '7', '7', '0'),
('21', 'Ten Steps Towards Systematic Requirements Reuse', 'ten-steps-towards-systematic-requirements-reuse', '<p style=\"text-align:justify\">Reusability is widely suggested to be a key to improving software development productivity and quality [1,2]. It has been further argued that reuse at the requirements level can significantly increase reuse at the later stages of development. However, there is little evidence in the literature to suggest that requirements reuse is widely practised. This paper describes ten practical steps towards systematic requirements reuse based on work at the Rolls-Royce Systems and Software University Technology Centre (UTC) for Rolls-Smiths Engine Controls Ltd (RoSEC) in the domain of aero-engine control systems. We believe these steps have made a significant overall contribution to the 50% reuse figure quoted by the management at RoSEC for current projects within the BR700 family of engine controllers.</p>', NULL, '2022-01-24 18:08:08', '2022-01-24 18:08:08', 'Ten Steps Towards Systematic Requirements Reuse.pdf', '8', '9', '0');

INSERT INTO `socials` (`id`, `title`, `icon`, `link`, `created_at`, `updated_at`) VALUES
('1', 'Facebook', '<svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fab\" data-icon=\"facebook\" class=\"svg-inline--fa fa-facebook fa-w-16\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 512 512\"><path fill=\"currentColor\" d=\"M504 256C504 119 393 8 256 8S8 119 8 256c0 123.78 90.69 226.38 209.25 245V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.28c-30.8 0-40.41 19.12-40.41 38.73V256h68.78l-11 71.69h-57.78V501C413.31 482.38 504 379.78 504 256z\"></path></svg>', '#', '2021-11-28 17:44:27', '2021-12-04 14:56:39'),
('2', 'Twitter', '<svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fab\" data-icon=\"twitter\" class=\"svg-inline--fa fa-twitter fa-w-16\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 512 512\"><path fill=\"currentColor\" d=\"M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z\"></path></svg>', '#', '2021-11-28 17:44:38', '2021-12-04 14:57:09'),
('3', 'Instagram', '<svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fab\" data-icon=\"instagram\" class=\"svg-inline--fa fa-instagram fa-w-14\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 448 512\"><path fill=\"currentColor\" d=\"M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z\"></path></svg>', '#', '2021-11-28 17:44:49', '2021-12-04 14:57:32'),
('4', 'Linkedin', '<svg aria-hidden=\"true\" focusable=\"false\" data-prefix=\"fab\" data-icon=\"linkedin-in\" class=\"svg-inline--fa fa-linkedin-in fa-w-14\" role=\"img\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 448 512\"><path fill=\"currentColor\" d=\"M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z\"></path></svg>', '#', '2021-12-04 14:56:19', '2021-12-04 14:56:19');

INSERT INTO `submissions` (`id`, `name`, `email`, `subject`, `file`, `approved`, `message`, `created_at`, `updated_at`) VALUES
('1', 'John Doe', 'john@gmail.com', 'Test Submissiopj', 'Screenshot 2022-04-19 at 09.39.57.png', '1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2022-04-19 20:31:24', '2022-04-19 20:43:44'),
('2', 'Arif Mammadov', 'arif@yandex.cOM', 'Salam', 'website-SAMPLE-proposal.pdf', '0', 'Test', '2022-04-23 17:47:49', '2022-04-23 17:47:49');

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
('1', 'Gasim Mammadov', 'info@arifmammadov.com', NULL, '$2y$10$biQamyrhp.OMR0vJWuke4.L.IuFNmEV5FrP/ebpXLbbq8mhVZUoEC', 'DLkoNWkIFBAs7c9G4NBT5kqlyHGCh9fOqSL3fyqGpJTt1WaECTgKm2ITzxGN', '2021-11-28 13:57:16', '2021-11-28 13:57:16'),
('2', 'Gasim Mammadov', 'Qasimbaku@hotmail.com', NULL, '$2y$10$z3cp5mD32DbzMxfDziohHuMgmNUaKKfmFXQmK2JKkUyGEi7NDv.vK', 'oQqfv1QFKpaAoIx8ebkBZWzeqhvVcWgnvjds44F69OHYNhTwKVOzeV1fh4uD', '2022-01-22 20:35:04', '2022-01-22 20:35:04');



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;