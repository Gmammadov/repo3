<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', '\App\Http\Controllers\PagesController@index');
Route::get('/about', '\App\Http\Controllers\PagesController@about');
Route::get('/contact', '\App\Http\Controllers\PagesController@contact');
Route::get('/posts', '\App\Http\Controllers\PagesController@posts');
Route::get('/posts/{slug}', '\App\Http\Controllers\PagesController@post');
Route::get('/stories', '\App\Http\Controllers\PagesController@stories');
Route::post('/comments/{post_id}', '\App\Http\Controllers\PagesController@comment')->name('addComments');
Route::post('/stories/send', '\App\Http\Controllers\PagesController@store')->name('pages.store');
Route::post('/submissions', '\App\Http\Controllers\PagesController@submit')->name('submissions.post');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::middleware(['auth'])->prefix('dashboard')->group(function () {
    Route::resource('/socials', '\App\Http\Controllers\SocialsController');
    Route::resource('/categories', '\App\Http\Controllers\CategoriesController');
    Route::resource('/authors', '\App\Http\Controllers\AuthorsController');
    Route::resource('/stories', '\App\Http\Controllers\StoriesController');
    Route::resource('/slides', '\App\Http\Controllers\SlidesController');
    Route::resource('/posts', '\App\Http\Controllers\PostsController');
    Route::resource('/comments', '\App\Http\Controllers\CommentsController');
    Route::resource('/submissions', '\App\Http\Controllers\SubmissionsController');
    Route::get('/posts/deleteImage/{id}', '\App\Http\Controllers\PostsController@deleteImage');
});

require __DIR__.'/auth.php';
